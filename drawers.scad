tolerance=.7;	

drawersx=80;
drawersy=60;
drawersz=40;

nclipsx=2;
nclipsy=1;
nclipsz=1;

ndrawers=2;
wall = 1.5;

lockLength=6;

sqrt2=1.414213562373095048801688724;

// constraint: 10*wall <= (3/8)*min(drawersx,drawersz) 


gluespace=5*wall;
sliderDepth=2*wall;
boxx = drawersx-2*wall;	
boxy = drawersy-2*sliderDepth-2*wall-2*gluespace;	
boxz = (drawersz -4*wall) / ndrawers;
deep = boxz-wall-tolerance;	// Depth of compartments
handleSize=boxz/2;

module slider(l)
{
	cube (size=[l,sliderDepth,sliderDepth],center=false);
	translate ([0,0,deep+wall-sliderDepth*2-tolerance]) cube(size=[lockLength,sliderDepth,sliderDepth],center=false);
	translate ([0,0,deep+wall-sliderDepth*3])
		intersection() {
			cube(size=[lockLength,sliderDepth,sliderDepth],center=false);
			translate([0,sliderDepth,0]) rotate([45,0,0]) cube(size=[lockLength,sliderDepth*2,sliderDepth*2],center=false);
		}  
}

module sliderhole(l)
{
	cube (size=[l,sliderDepth+tolerance,sliderDepth+tolerance],center=false);
	translate ([0,0,sliderDepth*2]) cube(size=[l-sliderDepth,sliderDepth+tolerance,deep-2*sliderDepth],center=false);
	translate ([0,0,deep+wall-sliderDepth*3])
		intersection() {
			cube(size=[l-sliderDepth,sliderDepth+tolerance,sliderDepth+tolerance],center=false);
			translate([0,sliderDepth,0]) rotate([45,0,0]) cube(size=[l-sliderDepth,sliderDepth*2+tolerance,sliderDepth*2+tolerance],center=false);
		}  
}


module drawer(nx,ny)
{
	compx=(boxx-wall)/nx-wall;
	compy=(boxy-wall)/ny-wall;
	difference()
	{
		cube([boxx,boxy,(deep+wall)],center=false);
		for ( ybox = [ 0 : ny - 1])    
		{
			for( xbox = [ 0 : nx - 1])
			{        
				translate([xbox*(compx+wall)+wall,ybox*(compy+wall)+wall,wall]) 
					cube([compx,compy,boxz],center=false);
			}
		}
	}
	translate([0,-sliderDepth,0]) slider(boxx);
	translate([0,boxy+sliderDepth,0]) 
		mirror([0,1,0]) 
		slider(boxx);
	// handle
	translate([boxx,boxy/2,(deep+wall)/2]) 
	difference() 
	{
		intersection()
		{
			union() {
				rotate([90,0,0])
					cylinder(r=sqrt2*handleSize/2,h=handleSize/2,center=true);
				rotate([0,45,0])
					cube([handleSize,handleSize/2,handleSize],center=true);
			}  
			rotate([0,45,0])
				translate([-handleSize/2,0,0])
					cube([handleSize*2,handleSize,handleSize*2],center=true);
			translate([handleSize-wall,0,0]) cube([handleSize*2,handleSize*2,handleSize*2],center=true);

		}
		difference()
		{
			rotate([90,0,0]) 
				cylinder(r=handleSize/3,h=handleSize,center=true,$fn=10);
			translate([handleSize/2,0,-handleSize/2])		
				rotate([0,45,0])
					cube([handleSize,handleSize,handleSize],center=true);
		}
	}
}
module leftside()
{
	difference()
	{
		cube([boxx,sliderDepth+wall+gluespace,boxz*ndrawers],center=false);
		intersection()
		{
			translate([wall,0,wall]) cube([boxx-2*wall,gluespace,drawersz-6*wall],center=false);
			union() {
				for(x=[0:wall*3:boxx*2])
				{
					translate([x,0,0]) rotate([0,-135,0]) cube([max(boxx,boxy),gluespace,wall],center=false); 
				}
	/*			for(x=[-boxx:wall*3:boxx])
				{
					translate([x,0,0]) rotate([0,-45,0]) cube([boxx+boxy,gluespace,wall],center=false); 
				}rallenta un sacco la compilazione..*/ 
			}
		}
		for(i=[0:ndrawers])
		{
			translate([0,gluespace+wall,i*(deep+wall+tolerance)]) sliderhole(boxx);
		}
		for(i=[0:nclipsx-1])
		{
			translate([(i+1)*(boxx/(nclipsx+1)),2*wall,-2*wall]) rotate([90,90,180]) clipbb();
		}
		for(i=[0:nclipsx-1])
		{
			translate([(i+1)*(boxx/(nclipsx+1)),2*wall,boxz*ndrawers+2*wall]) rotate([90,-90,180]) clipbb();
		}
		for(i=[0:nclipsz-1])
		{
			translate([-2*wall,2*wall,(i+1)*(boxz*ndrawers/(nclipsz+1))]) rotate([90,0,180]) clipbb();
		}
	}  

}
module rightside()
{
	mirror([0,1,0]) leftside();
}

module backside()
{
	difference()
	{	
		cube([2*wall,drawersy,drawersz]);
		for(i=[0:nclipsz-1])
		{
			translate([0,2*wall,(i+1)*(boxz*ndrawers/(nclipsz+1))+2*wall]) rotate([90,0,180]) clipbb();
		}
		for(i=[0:nclipsz-1])
		{
			translate([0,drawersy-2*wall,(i+1)*(boxz*ndrawers/(nclipsz+1))+2*wall]) rotate([-90,0,180]) clipbb();
		}
	}
}
module topbottomside()
{
	difference() {
		cube([drawersx-2*wall,drawersy,2*wall]);
		for(i=[0:nclipsx-1])
		{
			translate([(i+1)*(boxx/(nclipsx+1)),2*wall,2*wall]) rotate([-90,-90,0]) clipbb();
		}
		for(i=[0:nclipsx-1])
		{
			translate([(i+1)*(boxx/(nclipsx+1)),drawersy-2*wall,2*wall]) rotate([90,-90,0]) clipbb();
		}
	}
		
}

clipx=10*wall;

module clipbb()
{
	translate([-clipx/2-wall/2,0,0])
		union() {
			cube([clipx+tolerance,5*wall+tolerance,wall*2+tolerance],center=true);
			translate([clipx/2,0,wall/2])
				cube([wall+tolerance,8*wall+tolerance,wall*3+tolerance],center=true);
			intersection() {
				translate([-clipx/2-3*wall,0,0])
					cube([6*wall+tolerance,7*wall+tolerance,wall*2+tolerance],center=true);
				translate([-clipx/2,0,0])
					rotate([0,0,45])
					cylinder(r=3.5*wall+tolerance,h=2*wall+tolerance,center=true,$fn=50);
			}
		}
}
module clip()
{
	difference() {
		translate([-clipx/2-wall/2,0,0])
			union() {
				cube([clipx,5*wall,wall*2],center=true);
				translate([clipx/2,0,wall/2])
					cube([wall,8*wall,wall*3],center=true);
				intersection() {
					translate([-clipx/2-3*wall,0,0])
						cube([6*wall,7*wall,wall*2],center=true);
					translate([-clipx/2,0,0])
						rotate([0,0,45])
						cylinder(r=3.5*wall,h=2*wall,center=true,$fn=50);
				}
		}
		translate([-clipx/2-wall/2,0,0])
			union()
			{
				translate([-clipx/2,0,0])
					cube([clipx*3/2,3*wall,3*wall],center=true);
				translate([clipx/4,0,0])
					cylinder(r=wall*1.5,h=3*wall,center=true,$fn=10);
				translate([-clipx-3*wall,0,0]) 
					scale([2,1,1])
						rotate([0,0,45])
				#			cube([7*wall,7*wall,3*wall],center=true);
			}
	}
}

module cliptest()
{
	difference() {
		translate([-clipx/2-wall/2,0,0])
			cube([clipx+wall,12*wall,6*wall],center=true);
		clipbb();

	}

}

// TARGETS: ##drawer1 ##drawer2 ##top ##bottom ##left ##right ##back ##clip ##testblock
drawer(1,1); //##drawer1
translate ([0,0,deep+wall+tolerance]) drawer(1,2); //##drawer2
translate ([0,-12,wall*2]) leftside(); //##left
translate ([0,52,-tolerance/2]) rightside(); //##right
translate ([-5,-10,0]) backside(); //##back
translate ([0,-10,drawersz]) topbottomside();// ##top
translate ([0,-10,-5]) topbottomside();// ##bottom
translate([0,10,0]) clip();// ##clip
cliptest();// ##testblock

